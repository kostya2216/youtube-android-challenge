package com.kostya.test.rest.services;

import com.kostya.test.rest.responses.PlaylistResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Kostyantin on 4/6/2017.
 */
public interface PlaylistService {

    @GET("youtube-api.json")
    Call<PlaylistResponse> getPlaylist();

}

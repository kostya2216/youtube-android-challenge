package com.kostya.test.rest.responses;

import com.kostya.test.data.PlayList;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlaylistResponse {

    private List<PlayList> Playlists;

    public PlaylistResponse(List<PlayList> playlists) {
        Playlists = playlists;
    }

    public List<PlayList> getPlaylists() {
        return Playlists;
    }

    public void setPlaylists(List<PlayList> playlists) {
        Playlists = playlists;
    }

    @Override
    public String toString() {
        return "PlaylistResponse{" +
                "Playlists=" + Playlists +
                '}';
    }
}

package com.kostya.test.ui.acitvities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.kostya.test.R;
import com.kostya.test.controllers.ActivityController;
import com.kostya.test.ui.fragments.BaseFragment;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public abstract class BaseActivity<C extends ActivityController> extends AppCompatActivity implements BaseFragment.BaseFragmentInteractionsListener<C> {

    private C mController;

    public abstract C createController();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mController = createController();
        super.onCreate(savedInstanceState);
    }

    @Override
    public C getController() {
        return mController;
    }

    @Override
    public void startChrome(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            // Chrome browser presumably not installed so allow user to choose instead
            intent.setPackage(null);

            try {
                startActivity(intent);
            } catch(ActivityNotFoundException innerEx) {
                // No web browser apps installed.
            }
        }
    }

    @Override
    public void handleError(Throwable t, boolean critical) {
        if(critical) {
            Toast.makeText(this, getString(R.string.critical_error), Toast.LENGTH_LONG).show();
            finishAffinity();
        }
    }

    @Override
    public void showVideo(String videoId) {
        Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, getString(R.string.youtube_api_key), videoId);
        startActivity(intent);
    }
}

package com.kostya.test.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kostya.test.R;
import com.kostya.test.controllers.PlayListController;
import com.kostya.test.data.PlayList;
import com.kostya.test.ui.adapters.CommonRecyclerAdapter;
import com.kostya.test.ui.adapters.PlaylistRecyclerAdapter;

import java.util.Collection;
import java.util.List;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlayListsListFragment extends StaticRecyclerListFragment<PlayListController> implements CommonRecyclerAdapter.CommonRecyclerViewInteractionsListener<PlayList> {

    private PlaylistSelectionListener mListener;

    public static PlayListsListFragment newInstance() {
        return new PlayListsListFragment();
    }

    public boolean isPopulated() {
        return getAdapter() != null;
    }

    public void populate(List<PlayList> playLists) {
        PlaylistRecyclerAdapter adapter = new PlaylistRecyclerAdapter(playLists, this);
        setAdapter(adapter);
    }

    @Override
    public void onItemClick(PlayList item) {
        if(mListener != null) {
            mListener.onPlaylistSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof PlaylistSelectionListener) {
            mListener = (PlaylistSelectionListener) context;
        } else {
            throw new IllegalArgumentException("context must implement " + PlaylistSelectionListener.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    public interface PlaylistSelectionListener {
        void onPlaylistSelected(PlayList playList);
    }

}

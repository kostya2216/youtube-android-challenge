package com.kostya.test.ui.acitvities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.kostya.test.R;
import com.kostya.test.controllers.PlayListController;
import com.kostya.test.data.PlayList;
import com.kostya.test.events.PlayListsLoadedEvent;
import com.kostya.test.ui.fragments.PlayListsDataModelFragment;
import com.kostya.test.ui.fragments.PlayListsListFragment;
import com.kostya.test.ui.fragments.PlaylistItemsListFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class PlayListActivity extends BaseActivity<PlayListController> implements PlayListsListFragment.PlaylistSelectionListener {

    private final static String FRAG_TAG_MODEL = "ModelFrag";
    private final static String FRAG_TAG_PLAYLIST_LIST = "PlayListList";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);

        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_holder, PlayListsListFragment.newInstance(), FRAG_TAG_PLAYLIST_LIST)
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        FragmentManager fm = getSupportFragmentManager();
        PlayListsListFragment playlistListFragment = (PlayListsListFragment) fm.findFragmentByTag(FRAG_TAG_PLAYLIST_LIST);
        if(!playlistListFragment.isPopulated()) {
            PlayListsDataModelFragment modelFragment = (PlayListsDataModelFragment) fm.findFragmentByTag(FRAG_TAG_MODEL);

            if (modelFragment == null) {
                modelFragment = PlayListsDataModelFragment.newInstance();
                fm.beginTransaction().add(modelFragment, FRAG_TAG_MODEL).commit();
            } else if (modelFragment.getPlayLists() != null) {
                playlistListFragment.populate(new ArrayList<>(modelFragment.getPlayLists()));
            }
        }
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlayListsLoadedEvent(PlayListsLoadedEvent event) {
        PlayListsListFragment playlistListFragment = (PlayListsListFragment) getSupportFragmentManager().findFragmentByTag(FRAG_TAG_PLAYLIST_LIST);
        if(playlistListFragment != null) {
            playlistListFragment.populate(new ArrayList<>(event.playListMap.values()));
        }
    }

    @Override
    public PlayListController createController() {
        return new PlayListController();
    }

    @Override
    public void onPlaylistSelected(PlayList playList) {
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .add(R.id.fragment_holder, PlaylistItemsListFragment.newInstance(playList))
                .commit();
    }
}

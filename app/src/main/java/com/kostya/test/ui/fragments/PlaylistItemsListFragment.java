package com.kostya.test.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kostya.test.R;
import com.kostya.test.controllers.PlayListController;
import com.kostya.test.data.PlayList;
import com.kostya.test.data.PlaylistItem;
import com.kostya.test.ui.adapters.CommonRecyclerAdapter;
import com.kostya.test.ui.adapters.PlaylistItemRecyclerAdapter;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlaylistItemsListFragment extends StaticRecyclerListFragment<PlayListController> implements CommonRecyclerAdapter.CommonRecyclerViewInteractionsListener<PlaylistItem> {

    public final static String ARG_PLAYLIST = "playlist";

    public static PlaylistItemsListFragment newInstance(PlayList playList) {
        PlaylistItemsListFragment fragment = new PlaylistItemsListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PLAYLIST, playList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PlaylistItemRecyclerAdapter adapter = new PlaylistItemRecyclerAdapter(getPlayList().getListItems(), this);
        setAdapter(adapter);
        setBackground(R.color.colorBackground);
    }

    public PlayList getPlayList() {
        return getArguments().getParcelable(ARG_PLAYLIST);
    }

    @Override
    public void onItemClick(PlaylistItem item) {
        showVideo(item.getVideoId());
    }
}

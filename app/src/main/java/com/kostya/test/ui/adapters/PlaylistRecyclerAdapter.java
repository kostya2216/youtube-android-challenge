package com.kostya.test.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kostya.test.R;
import com.kostya.test.data.PlayList;
import com.kostya.test.ui.adapters.PlaylistRecyclerAdapter.PlayListViewHolder;
import com.kostya.test.ui.adapters.CommonRecyclerAdapter.CommonViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlaylistRecyclerAdapter extends CommonRecyclerAdapter<PlayList, PlayListViewHolder> {

    public PlaylistRecyclerAdapter(List<PlayList> items, CommonRecyclerViewInteractionsListener<PlayList> listener) {
        super(items, listener);
    }

    @Override
    public PlayListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_playlist, parent, false);
        PlayListViewHolder vh = new PlayListViewHolder(v);

        attachItemClickListener(v, vh);

        return vh;
    }

    class PlayListViewHolder extends CommonViewHolder<PlayList> {

        final ImageView ivBackground;
        final TextView tvTitle;

        public PlayListViewHolder(View itemView) {
            super(itemView);

            ivBackground = (ImageView) itemView.findViewById(R.id.iv_background);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }

        @Override
        public void populate(PlayList playList) {
            tvTitle.setText(playList.getListTitle());
            final String thumb = playList.getListItems().get(0).getThumb();
            itemView.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(itemView.getContext())
                            .load(thumb)
                            .centerCrop()
                            .resize(itemView.getWidth(), itemView.getHeight())
                            .into(ivBackground);
                }
            });
        }
    }
}

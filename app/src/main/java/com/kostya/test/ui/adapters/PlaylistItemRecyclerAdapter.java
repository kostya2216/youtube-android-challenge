package com.kostya.test.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kostya.test.R;
import com.kostya.test.data.PlaylistItem;
import com.kostya.test.ui.adapters.PlaylistItemRecyclerAdapter.PlaylistItemViewHolder;
import com.kostya.test.ui.adapters.CommonRecyclerAdapter.CommonViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlaylistItemRecyclerAdapter extends CommonRecyclerAdapter<PlaylistItem, PlaylistItemViewHolder> {

    public PlaylistItemRecyclerAdapter(List<PlaylistItem> items, CommonRecyclerViewInteractionsListener<PlaylistItem> listener) {
        super(items, listener);
    }

    @Override
    public PlaylistItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_playlist_item, parent, false);
        PlaylistItemViewHolder vh = new PlaylistItemViewHolder(v);

        attachItemClickListener(v, vh);

        return vh;
    }

    class PlaylistItemViewHolder extends CommonViewHolder<PlaylistItem> {

        final ImageView ivThumb;
        final TextView tvTitle;

        public PlaylistItemViewHolder(View itemView) {
            super(itemView);

            ivThumb = (ImageView) itemView.findViewById(R.id.iv_thumb);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }

        @Override
        public void populate(PlaylistItem item) {
            tvTitle.setText(item.getTitle());
            Picasso.with(itemView.getContext())
                    .load(item.getThumb())
                    .into(ivThumb);
        }
    }
}

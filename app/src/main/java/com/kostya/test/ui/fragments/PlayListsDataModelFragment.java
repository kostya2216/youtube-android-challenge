package com.kostya.test.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.kostya.test.controllers.PlayListController;
import com.kostya.test.data.PlayList;
import com.kostya.test.events.PlayListsLoadedEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Kostyantin on 4/6/2017.
 *
 *  Non - U.I. fragment, only for keeping playlist data alive on config changes.
 *
 */
public class PlayListsDataModelFragment extends BaseFragment<PlayListController> implements PlayListController.PlayListLoadCallback {

    private final AtomicReference<Map<String, PlayList>> mPlayListRef = new AtomicReference<>();

    public static PlayListsDataModelFragment newInstance() {
        return new PlayListsDataModelFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        getController().loadPlayLists(this);
    }

    public Collection<PlayList> getPlayLists() {
        Map<String, PlayList> playListMap = mPlayListRef.get();
        if(playListMap != null) {
            return playListMap.values();
        }
        return  null;
    }

    public PlayList getPlayList(String playListName) {
        Map<String, PlayList> playListMap = mPlayListRef.get();
        if(playListMap != null) {
            return playListMap.get(playListName);
        }
        return null;
    }

    @Override
    public void onPlayListsLoaded(List<PlayList> playLists) {
        Map<String, PlayList> playListMap = new HashMap<>();
        for(PlayList playList : playLists) {
            playListMap.put(playList.getListTitle(), playList);
        }
        mPlayListRef.set(playListMap);
        EventBus.getDefault().post(new PlayListsLoadedEvent(playListMap));
    }

    @Override
    public void onLoadingError(Throwable t) {
        handleError(t, true);
    }
}

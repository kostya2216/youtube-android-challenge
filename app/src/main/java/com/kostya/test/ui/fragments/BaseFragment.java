package com.kostya.test.ui.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.kostya.test.controllers.ActivityController;

/**
 * Created by Kostyantin on 4/6/2017.
 */
public class BaseFragment<C extends ActivityController> extends Fragment {

    private BaseFragmentInteractionsListener<C> mListener;

    public C getController() {
        if(mListener != null) {
            return mListener.getController();
        }
        return null;
    }

    public void startChrome(String url) {
        if(mListener != null) {
            mListener.startChrome(url);
        }
    }

    public void handleError(Throwable t, boolean critical) {
        if(mListener != null) {
            mListener.handleError(t, critical);
        }
    }

    public void showVideo(String videoId) {
        if(mListener != null) {
            mListener.showVideo(videoId);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof BaseFragmentInteractionsListener) {
            mListener = (BaseFragmentInteractionsListener<C>) context;
        } else {
            throw new IllegalArgumentException("context must implement " + BaseFragmentInteractionsListener.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    public interface BaseFragmentInteractionsListener<C extends ActivityController> {
        public C getController();
        public void startChrome(String url);
        public void handleError(Throwable t, boolean critical);
        public void showVideo(String videoId);
    }

}

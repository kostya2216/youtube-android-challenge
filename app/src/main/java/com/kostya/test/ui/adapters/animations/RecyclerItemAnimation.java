package com.kostya.test.ui.adapters.animations;

import android.view.animation.Animation;

public interface RecyclerItemAnimation {
    Animation getAnimation();
}
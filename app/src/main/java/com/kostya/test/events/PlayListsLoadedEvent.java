package com.kostya.test.events;

import com.kostya.test.data.PlayList;

import java.util.Map;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlayListsLoadedEvent {
    public final Map<String, PlayList> playListMap;

    public PlayListsLoadedEvent(Map<String, PlayList> playListMap) {
        this.playListMap = playListMap;
    }
}

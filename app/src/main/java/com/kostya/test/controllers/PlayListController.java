package com.kostya.test.controllers;

import com.kostya.test.data.PlayList;
import com.kostya.test.factories.RetrofitFactory;
import com.kostya.test.rest.responses.PlaylistResponse;
import com.kostya.test.rest.services.PlaylistService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlayListController implements ActivityController {

    public void loadPlayLists(final PlayListLoadCallback callback) {
        Retrofit retrofit = RetrofitFactory.createRetrofitClient("http://www.razor-tech.co.il/hiring/");
        PlaylistService service = retrofit.create(PlaylistService.class);
        service.getPlaylist().enqueue(new Callback<PlaylistResponse>() {
            @Override
            public void onResponse(Call<PlaylistResponse> call, Response<PlaylistResponse> response) {
                callback.onPlayListsLoaded(response.body().getPlaylists());
            }

            @Override
            public void onFailure(Call<PlaylistResponse> call, Throwable t) {
                callback.onLoadingError(t);
            }
        });
    }

    public interface PlayListLoadCallback {
        void onPlayListsLoaded(List<PlayList> playLists);
        void onLoadingError(Throwable t);
    }

}

package com.kostya.test.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlayList implements Parcelable {

    private String ListTitle;
    private List<PlaylistItem> ListItems;

    public PlayList() { }

    public PlayList(String listTitle, List<PlaylistItem> listItems) {
        ListTitle = listTitle;
        ListItems = listItems;
    }

    public String getListTitle() {
        return ListTitle;
    }

    public void setListTitle(String listTitle) {
        ListTitle = listTitle;
    }

    public List<PlaylistItem> getListItems() {
        return ListItems;
    }

    public void setListItems(List<PlaylistItem> listItems) {
        ListItems = listItems;
    }

    @Override
    public String toString() {
        return "PlayList{" +
                "ListTitle='" + ListTitle + '\'' +
                ", ListItems=" + ListItems +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ListTitle);
        dest.writeTypedList(this.ListItems);
    }

    protected PlayList(Parcel in) {
        this.ListTitle = in.readString();
        this.ListItems = in.createTypedArrayList(PlaylistItem.CREATOR);
    }

    public static final Creator<PlayList> CREATOR = new Creator<PlayList>() {
        @Override
        public PlayList createFromParcel(Parcel source) {
            return new PlayList(source);
        }

        @Override
        public PlayList[] newArray(int size) {
            return new PlayList[size];
        }
    };
}

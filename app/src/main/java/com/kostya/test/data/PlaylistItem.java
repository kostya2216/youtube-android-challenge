package com.kostya.test.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kostyantin on 4/6/2017.
 */

public class PlaylistItem implements Parcelable {

    private String Title;
    private String link;
    private String thumb;

    public PlaylistItem() { }

    public PlaylistItem(String title, String link, String thumb) {
        Title = title;
        this.link = link;
        this.thumb = thumb;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getVideoId() {
        return link.substring(link.indexOf("v=") + 2);
    }

    @Override
    public String toString() {
        return "PlaylistItem{" +
                "Title='" + Title + '\'' +
                ", link='" + link + '\'' +
                ", thumb='" + thumb + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Title);
        dest.writeString(this.link);
        dest.writeString(this.thumb);
    }

    protected PlaylistItem(Parcel in) {
        this.Title = in.readString();
        this.link = in.readString();
        this.thumb = in.readString();
    }

    public static final Creator<PlaylistItem> CREATOR = new Creator<PlaylistItem>() {
        @Override
        public PlaylistItem createFromParcel(Parcel source) {
            return new PlaylistItem(source);
        }

        @Override
        public PlaylistItem[] newArray(int size) {
            return new PlaylistItem[size];
        }
    };

}
